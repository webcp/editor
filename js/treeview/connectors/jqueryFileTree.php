<?php
//
// jQuery File Tree PHP Connector
//
// Version 1.01
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 24 March 2008
//
// History:
//
// 1.01 - updated to work with foreign characters in directory/file names (12 April 2008)
// 1.00 - released (24 March 2008)
//
// Output a list of files for jQuery File Tree
//
$_POST['dir'] = urldecode($_POST['dir']);
//$_POST["dir"] = substr($_POST["dir"], 0, strlen($_POST["dir"]) - 11);

if (substr($_POST["dir"], strlen($_POST["dir"]) - 1, 1) != "/") {
	$_POST["dir"] .= "/";
}

//file_put_contents(dirname(__FILE__)."/log.log", "dir: ".$_POST["dir"]."\r\n", FILE_APPEND);

if( file_exists($_POST['dir']) ) {
	$files = scandir($_POST['dir']);
	natcasesort($files);
	if( count($files) > 2 ) { /* The 2 accounts for . and .. */
		echo "<ul class=\"jqueryFileTree\" >";
		// All dirs
		foreach( $files as $file ) {
			if( $file != '.' && $file != '..' && file_exists($_POST['dir'] . $file) && is_dir($_POST['dir'] . $file) ) {
				echo "<li class=\"directory collapsed\"><a href=\"#\" rel=\"" . htmlentities($_POST['dir'] . $file) . "/\">" . htmlentities($file) . "</a></li>";
			}
		}

		echo "</ul>";	
	}
}

?>
