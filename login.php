<?php

session_start();
require_once($_SERVER["DOCUMENT_ROOT"]."/includes/functions.inc");

        if( ! isset($_GET["Nonce"]) )
        {
		print "Login failed or expired, please log into WebCP again and retry.";
                exit();
        }


        if( ! isset($_GET["TimeStamp"]) )
        {
		print "Login failed or expired, please log into WebCP again and retry..";
                exit();
        }


        $options2 = array(
                'uri' => 'http://localhost:8880',
                'location' => 'http://localhost:8880/API/nonce/nonce.php',
                'trace' => 1
        );


        $InputNonce = filter_var($_GET["Nonce"], FILTER_SANITIZE_STRING);

        $TimeStamp = filter_var($_GET["TimeStamp"], FILTER_SANITIZE_STRING);

        $MetaDataArray = array();
	array_push($MetaDataArray, $_SERVER["SERVER_ADDR"]);
        array_push($MetaDataArray, $_SERVER["SERVER_NAME"]);


        $client = new SoapClient(NULL, $options2);
        $Result = $client->VerifyNonce($InputNonce, "logInEditor", $TimeStamp, $MetaDataArray);

        if($Result == true)
        {
		SetLogin($InputNonce);
		header("location: index.php");
		exit();
	}

	print "<h1>Login failed or expired</h1>";
	print "The login link from WebCP is a one time only link. To log in again please go back to WebCP and click the editor link again";

